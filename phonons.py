from pathlib import Path

import ase.io.ulm as ulm
from ase.parallel import world
from ase.io import read
from ase.phonons import Phonons
from gpaw import GPAW


def phonons():
    state = Path().cwd().parts[-1]

    # Remove empty files:
    if world.rank == 0:
        for f in Path().glob('phonon.*.pckl'):
            if f.stat().st_size == 0:
                f.unlink()
    world.barrier()

    params = {}
    name = '../relax-{}.traj'.format(state)
    u = ulm.open(name)
    params.update(u[-1].calculator.parameters)
    u.close()
    params['symmetry'] = {'point_group': False,
                          'do_not_symmetrize_the_density': True}

    slab = read(name)
    calc = GPAW(txt='phonons.txt', **params)

    p = Phonons(slab, calc, supercell=(2, 2, 2))
    p.run()


if __name__ == '__main__':
    phonons()
