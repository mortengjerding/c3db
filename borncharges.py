import json
from os.path import exists
from os.path import splitext

import numpy as np
from gpaw import GPAW
from gpaw.mpi import world, serial_comm
from c3db.gs import write_gpw_file

from ase.parallel import paropen
from ase.units import Bohr
from ase.io import jsonio
from ase.dft.kpoints import get_monkhorst_pack_size_and_offset


def get_overlap(calc, bands, u1_nR, u2_nR, P1_ani, P2_ani, dO_aii, bG_v):
    M_nn = np.dot(u1_nR.conj(), u2_nR.T) * calc.wfs.gd.dv
    cell_cv = calc.wfs.gd.cell_cv
    r_av = np.dot(calc.spos_ac, cell_cv)

    for ia in range(len(P1_ani)):
        P1_ni = P1_ani[ia][bands]
        P2_ni = P2_ani[ia][bands]
        phase = np.exp(-1.0j * np.dot(bG_v, r_av[ia]))
        dO_ii = dO_aii[ia]
        M_nn += P1_ni.conj().dot(dO_ii).dot(P2_ni.T) * phase

    return M_nn


def get_berry_phases(calc, spin=0, dir=0, check2d=False):
    if isinstance(calc, str):
        calc = GPAW(calc, communicator=serial_comm, txt=None)
    nocc = int(calc.wfs.setups.nvalence / 2)
    bands = list(range(nocc))
    kpts_kc = calc.get_bz_k_points()
    size = get_monkhorst_pack_size_and_offset(kpts_kc)[0]
    Nk = len(kpts_kc)
    wfs = calc.wfs
    icell_cv = (2 * np.pi) * np.linalg.inv(calc.wfs.gd.cell_cv).T

    dO_aii = []
    for ia, id in enumerate(wfs.setups.id_a):
        dO_ii = calc.wfs.setups[ia].dO_ii
        dO_aii.append(dO_ii)

    kd = calc.wfs.kd
    nik = kd.nibzkpts

    u_knR = []
    P_kani = []
    for k in range(Nk):
        ik = kd.bz2ibz_k[k]
        k_c = kd.bzk_kc[k]
        ik_c = kd.ibzk_kc[ik]
        kpt = wfs.kpt_u[ik + spin * nik]
        psit_nG = kpt.psit_nG
        ut_nR = wfs.gd.empty(nocc, wfs.dtype)
        for n in range(nocc):
            ut_nR[n] = wfs.pd.ifft(psit_nG[n], ik)

        sym = kd.sym_k[k]
        U_cc = kd.symmetry.op_scc[sym]
        time_reversal = kd.time_reversal_k[k]
        sign = 1 - 2 * time_reversal
        phase_c = k_c - sign * np.dot(U_cc, ik_c)
        phase_c = phase_c.round().astype(int)

        N_c = wfs.gd.N_c

        if not (U_cc == np.eye(3)).all():
            i_cr = np.dot(U_cc.T, np.indices(N_c).reshape((3, -1)))
            i = np.ravel_multi_index(i_cr, N_c, 'wrap')

            for ut_R in ut_nR:
                tmp_R = ut_R.ravel()[i].reshape(N_c)
                ut_R[:] = tmp_R

        if time_reversal:
            ut_nR = ut_nR.conj()

        if np.any(phase_c):
            emikr_R = np.exp(-2j * np.pi *
                             np.dot(np.indices(N_c).T, phase_c / N_c).T)
            u_knR.append(ut_nR * emikr_R[np.newaxis])
        else:
            u_knR.append(ut_nR)

        a_a = []
        U_aii = []
        for a, id in enumerate(wfs.setups.id_a):
            b = kd.symmetry.a_sa[sym, a]
            S_c = np.dot(calc.spos_ac[a], U_cc) - calc.spos_ac[b]
            x = np.exp(2j * np.pi * np.dot(k_c, S_c))
            U_ii = wfs.setups[a].R_sii[sym].T * x
            a_a.append(b)
            U_aii.append(U_ii)

        P_ani = []
        for b, U_ii in zip(a_a, U_aii):
            P_ni = np.dot(kpt.P_ani[b][:nocc], U_ii)
            if time_reversal:
                P_ni = P_ni.conj()
            P_ani.append(P_ni)

        P_kani.append(P_ani)

    indices_kkk = np.arange(Nk).reshape(size)
    tmp = np.concatenate([[i for i in range(3) if i != dir], [dir]])
    indices_kk = indices_kkk.transpose(tmp).reshape(-1, size[dir])

    nkperp = len(indices_kk)
    phases = []
    if check2d:
        phases2d = []
    for indices_k in indices_kk:
        M_knn = []
        for j in range(size[dir]):
            k1 = indices_k[j]
            G_c = np.array([0, 0, 0])
            if j + 1 < size[dir]:
                k2 = indices_k[j + 1]
            else:
                k2 = indices_k[0]
                G_c[dir] = 1
            u1_nR = u_knR[k1]
            u2_nR = u_knR[k2]
            k1_c = kpts_kc[k1]
            k2_c = kpts_kc[k2] + G_c

            if np.any(G_c):
                emiGr_R = np.exp(-2j * np.pi *
                                 np.dot(np.indices(N_c).T, G_c / N_c).T)
                u2_nR = u2_nR * emiGr_R

            bG_c = k2_c - k1_c
            bG_v = np.dot(bG_c, icell_cv)
            M_nn = get_overlap(calc,
                               bands,
                               np.reshape(u1_nR, (nocc, -1)),
                               np.reshape(u2_nR, (nocc, -1)),
                               P_kani[k1],
                               P_kani[k2],
                               dO_aii,
                               bG_v)
            M_knn.append(M_nn)
        det = np.linalg.det(M_knn)
        phases.append(np.imag(np.log(np.prod(det))))
        if check2d:
            # In the case of 2D systems we can check the
            # result
            k1 = indices_k[0]
            k1_c = kpts_kc[k1]
            G_c = [0, 0, 1]
            G_v = np.dot(G_c, icell_cv)
            u1_nR = u_knR[k1]
            emiGr_R = np.exp(-2j * np.pi *
                             np.dot(np.indices(N_c).T, G_c / N_c).T)
            u2_nR = u1_nR * emiGr_R

            M_nn = get_overlap(calc,
                               bands,
                               np.reshape(u1_nR, (nocc, -1)),
                               np.reshape(u2_nR, (nocc, -1)),
                               P_kani[k1],
                               P_kani[k1],
                               dO_aii,
                               G_v)
            phase2d = np.imag(np.log(np.linalg.det(M_nn)))
            phases2d.append(phase2d)

    # Make sure the phases are continuous
    for p in range(nkperp - 1):
        delta = phases[p] - phases[p + 1]
        phases[p + 1] += np.round(delta / (2 * np.pi)) * 2 * np.pi

    phase = np.sum(phases) / nkperp
    if check2d:
        for p in range(nkperp - 1):
            delta = phases2d[p] - phases2d[p + 1]
            phases2d[p + 1] += np.round(delta / (2 * np.pi)) * 2 * np.pi

        phase2d = np.sum(phases2d) / nkperp

        diff = abs(phase - phase2d)
        if diff > 0.01:
            msg = 'Warning wrong phase: phase={}, 2dphase={}'
            print(msg.format(phase, phase2d))

    return indices_kk, phases


def get_wavefunctions(atoms, name, params):
    params['kpts']['density'] = 6.0
    tmp = splitext(name)[0]
    atoms.calc = GPAW(txt=tmp + '.txt', **params)
    atoms.get_potential_energy()
    atoms.calc.write(name, 'all')
    return atoms.calc


def borncharges(delta=0.01):
    if not exists('gs.gpw'):
        # Calculate ground state
        write_gpw_file()

    calc = GPAW('gs.gpw', txt=None)
    params = calc.parameters
    atoms = calc.atoms
    cell_cv = atoms.get_cell() / Bohr
    vol = abs(np.linalg.det(cell_cv))
    sym_a = atoms.get_chemical_symbols()

    Z_a = []
    for num in calc.atoms.get_atomic_numbers():
        for ida, setup in zip(calc.wfs.setups.id_a,
                              calc.wfs.setups):
            if abs(ida[0] - num) < 1e-5:
                break
        Z_a.append(setup.Nv)
    Z_a = np.array(Z_a)

    # List for atomic indices
    indices = list(range(len(sym_a)))

    pos_av = atoms.get_positions()
    avg_v = np.sum(pos_av, axis=0) / len(pos_av)
    pos_av -= avg_v
    atoms.set_positions(pos_av)
    Z_avv = []
    norm_c = np.linalg.norm(cell_cv, axis=1)
    proj_cv = cell_cv / norm_c[:, np.newaxis]

    B_cv = np.linalg.inv(cell_cv).T * 2 * np.pi
    area_c = np.zeros((3,), float)
    area_c[[2, 1, 0]] = [np.linalg.norm(np.cross(B_cv[i], B_cv[j]))
                         for i in range(3)
                         for j in range(3) if i < j]

    if world.rank == 0:
        print('a v s c phase')
    for a in indices:
        phase_scv = np.zeros((2, 3, 3), float)
        for v in range(3):
            for s, sign in enumerate([-1, 1]):
                # Update atomic positions
                atoms.positions = pos_av
                atoms.positions[a, v] = pos_av[a, v] + sign * delta
                name = '{}{}{}-displace.gpw'.format(a, 'xyz'[v], ' +-'[sign])

                if not exists(name):
                    calc = get_wavefunctions(atoms, name, params)
                for c in [0, 1, 2]:
                    # Check if 2D
                    check2d = (v == 2) * (c == 2)
                    indices_kk, phases = get_berry_phases(name, dir=c,
                                                          check2d=check2d)
                    phase = np.sum(phases) / len(indices_kk)
                    if world.rank == 0:
                        print(sym_a[a], a, v, s, c, phase)
                    phase_scv[s, c, v] = phase

        dphase_cv = (phase_scv[1] - phase_scv[0])
        dphase_cv -= np.round(dphase_cv / (2 * np.pi)) * 2 * np.pi
        nspins = 2
        dP_cv = (nspins * area_c[:, np.newaxis] / (2 * np.pi)**3 *
                 dphase_cv)
        dP_vv = np.dot(proj_cv.T, dP_cv)
        Ze_vv = dP_vv * vol / (2 * delta / Bohr)
        Z_vv = Ze_vv + Z_a[a] * np.eye(3)
        if world.rank == 0:
            print('phase_scc')
            print(np.round(phase_scv, 3))
            print('dphase_cc')
            print(np.round(dphase_cv, 3))
            print('dP_vv')
            print(np.round(dP_vv, 5))
            print('Ze_vv')
            print(np.round(Ze_vv, 2))
            print('Z_vv')
            print(np.round(Z_vv, 2))
        Z_avv.append(Z_vv)

    data = {'Z_avv': Z_avv, 'indices_a': indices, 'sym_a': sym_a}

    filename = 'borncharges.json'

    with paropen(filename, 'w') as fd:
        json.dump(jsonio.encode(data), fd)

if __name__ == '__main__':
    borncharges()
