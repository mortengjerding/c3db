from gpaw import GPAW
import json
from ase.parallel import paropen
from ase.io import jsonio
import os.path as op


def nonsc(kdens=12, emptybands=20, outname='densk'):
    """Non self-consistent calculation with dense k-point sampling
       based on the density in gs.gpw
    """
    if op.isfile(outname + '.gpw'):
        return GPAW(outname + '.gpw', txt=None)

    calc = GPAW('gs.gpw', txt=None)

    def get_kpts_size(atoms, density):
        """trying to get a reasonable monkhorst size which hits high
        symmetry points
        """
        from gpaw.kpt_descriptor import kpts2sizeandoffsets as k2so
        size, offset = k2so(atoms=calc.atoms, density=density)
        for i in range(3):
            if size[i] % 6 != 0:
                size[i] = 6 * (size[i] // 6 + 1)

        kpts = {'size': size, 'gamma': True}
        return kpts

    kpts = get_kpts_size(atoms=calc.atoms, density=kdens)
    convbands = int(emptybands / 2)
    calc.set(nbands=-emptybands,
             basis='dzp',
             txt=outname + '.txt',
             fixdensity=True,
             kpts=kpts,
             convergence={'bands': -convbands})

    calc.get_potential_energy()
    calc.write(outname + '.gpw')
    return calc


def calcdos(gpwname='densk.gpw'):
    """
    Get dos at ef
    """
    if not op.exists(gpwname):
        nonsc()

    calc = GPAW(gpwname, txt=None)

    ns = calc.get_number_of_spins()
    from ase.dft.dos import DOS
    dos = DOS(calc, width=0.0, window=(-10, 10), npts=2000)
    e = dos.get_energies()
    dos_spin0_e = dos.get_dos(spin=0)

    data = {'energies': e,
            'dos_spin0': dos_spin0_e}

    if ns == 2:
        dos_spin1_e = dos.get_dos(spin=1)
        data['dos_spin1'] = dos_spin1_e
    
    fname = 'dos.json'
    with paropen(fname, 'w') as fd:
        json.dump(jsonio.encode(data), fd)


if __name__ == '__main__':
    calcdos()
