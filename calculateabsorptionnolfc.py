import sys
from os.path import splitext, split, exists
from os import chdir
from gpaw import GPAW, FermiDirac
from gpaw.bztools import find_high_symmetry_monkhorst_pack
from gpaw.response.df import DielectricFunction
from gpaw.mpi import world
import numpy as np
import os.path as op
import os


def calculate():
    atomic_structure = sys.argv[1]
    dirname, filename = split(atomic_structure)
    compound = splitext(filename)[0]

    gs = compound + '_PBE_gs.gpw'
    if not exists(gs):
        gs = compound + '_PBE_gs_k10_t100.gpw'

    density = 20

    if not exists(compound + '_wfs_nolfc.gpw'):
        calc_old = GPAW(gs, txt=None)
        nval = calc_old.wfs.nvalence
        try:
            kpts = find_high_symmetry_monkhorst_pack(gs,
                                                     density=density)
        except RuntimeError:
            kpts = {'density': density, 'gamma': True, 'even': True}

        calc = GPAW(gs, kpts=kpts, nbands=nval + 10,
                    convergence={'bands': -10},
                    fixdensity=True)
        calc.get_potential_energy()
        # calc.diagonalize_full_hamiltonian(nbands=nval, expert=True)
        calc.write(compound + '_wfs_nolfc.gpw', mode='all')

    df = DielectricFunction(compound + '_wfs_nolfc.gpw', eta=1e-10,
                            domega0=0.02,
                            integrationmode='tetrahedron integration', ecut=1,
                            name=compound + '_chi_nolfc')

    df1x, df2x = df.get_dielectric_function(direction='x')
    df1y, df2y = df.get_dielectric_function(direction='y')
    df1z, df2z = df.get_dielectric_function(direction='z')

    plasmafreq_vv = df.chi0.plasmafreq_vv

    frequencies = df.get_frequencies()
    data = {'df1x': np.array(df1x),
            'df2x': np.array(df2x),
            'df1y': np.array(df1y),
            'df2y': np.array(df2y),
            'df1z': np.array(df1z),
            'df2z': np.array(df2z),
            'plasmafreq_vv': plasmafreq_vv,
            'frequencies': frequencies}

    filename = compound + '_df_nolfc.npz'

    if world.rank == 0:
        np.savez_compressed(filename, **data)

    world.barrier()

    if op.isfile(compound + '_wfs_nolfc.gpw') and op.isfile(filename):
        if world.rank == 0:
            os.remove(compound + '_wfs_nolfc.gpw')


if __name__ == '__main__':
    calculate()
