from pathlib import Path

from ase.io import read
from ase.io.ulm import open as ulmopen
from gpaw import GPAW, PW, FermiDirac


def write_gpw_file():
    state = Path().cwd().parts[-1]

    params = dict(
        mode=PW(800),
        xc='PBE',
        basis='dzp',
        kpts={'density': 6.0, 'gamma': True},
        occupations=FermiDirac(width=0.05))

    name = '../relax-{}.traj'.format(state)
    u = ulmopen(name)
    params.update(u[-1].calculator.get('parameters', {}))
    u.close()
    slab = read(name)
    slab.pbc = (1, 1, 1)
    slab.calc = GPAW(txt='gs.txt', **params)
    slab.get_forces()
    slab.get_stress()
    slab.calc.write('gs.gpw')


if __name__ == '__main__':
    write_gpw_file()
