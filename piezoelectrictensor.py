import json
from os.path import exists
from os.path import splitext

import numpy as np
from gpaw import GPAW
from gpaw.mpi import world, serial_comm
from c3db.gs import write_gpw_file

from ase.parallel import paropen
from ase.units import Bohr
from ase.io import jsonio
from c3db.borncharges import get_berry_phases
from c2db.bfgs import BFGS
from ase.io import read
from ase.constraints import FixAtoms


def get_polarization_phase(calc):
    if isinstance(calc, str):
        calc = GPAW(calc, communicator=serial_comm, txt=None)

    nspins = calc.wfs.nspins
    phase_c = np.zeros((3,), float)
    for c in [0, 1, 2]:
        for spin in range(nspins):
            indices_kk, phases = get_berry_phases(calc, dir=c, spin=spin)
            phase_c[c] += np.sum(phases) / len(indices_kk)

    phase_c = phase_c * 2 / nspins

    # Ionic contribution
    Z_a = []
    for num in calc.atoms.get_atomic_numbers():
        for ida, setup in zip(calc.wfs.setups.id_a,
                              calc.wfs.setups):
            if abs(ida[0] - num) < 1e-5:
                break
        Z_a.append(setup.Nv)
    Z_a = np.array(Z_a)
    phase_c += 2 * np.pi * np.dot(Z_a, calc.spos_ac)

    return phase_c


def get_wavefunctions(atoms, name, params):
    tmp = splitext(name)[0]
    atoms.calc = GPAW(txt=tmp + '.txt', **params)
    atoms.get_potential_energy()
    atoms.calc.write(name, 'all')
    return atoms.calc


def piezoelectrictensor(delta=0.001):
    if not exists('gs.gpw'):
        # Calculate ground state
        write_gpw_file()

    calc = GPAW('gs.gpw', txt=None)
    params = calc.parameters

    # From experience it is important to use
    # non-gamma centered grid when using symmetries.
    # Might have something to do with degeneracies, not sure.
    params['kpts'] = {'density': 8.0, 'gamma': False}
    params['symmetry'] = {'point_group': True,
                          'do_not_symmetrize_the_density': False,
                          'time_reversal': True}
    # We need the eigenstates to a higher accuracy
    params['convergence']['eigenstates'] = 1e-11
    atoms = calc.atoms
    oldcell_cv = atoms.get_cell()
    pbc_c = atoms.pbc
    if world.rank == 0:
        print('i j s')
    epsclamped_vvv = np.zeros((3, 3, 3), float)
    eps_vvv = np.zeros((3, 3, 3), float)

    for i in range(3):
        for j in range(3):
            if j < i:
                continue
            phaseclamped_sc = np.zeros((2, 3), float)
            phase_sc = np.zeros((2, 3), float)
            if world.rank == 0:
                print(i, j)
            for s, sign in enumerate([-1, 1]):
                if not pbc_c[i] or not pbc_c[j]:
                    continue

                # Update atomic structure
                strain_vv = np.zeros((3, 3), float)
                strain_vv[i, j] = sign * delta
                newcell_cv = np.dot(oldcell_cv,
                                    np.eye(3) + strain_vv)
                atoms.set_cell(newcell_cv, scale_atoms=True)
                namegpw = 'strain-{}{}{}.gpw'.format(i, j, '-+'[s])

                if not exists(namegpw):
                    calc = get_wavefunctions(atoms, namegpw, params)
                phaseclamped_sc[s] = get_polarization_phase(namegpw)

                # Now relax atoms
                relaxname = 'strain-{}{}{}-relaxed'.format(i, j, '-+'[s])
                relaxnamegpw = relaxname + '.gpw'
                if not exists(relaxnamegpw):
                    relaxedatoms = atoms.copy()
                    constraint = FixAtoms(indices=[0])
                    relaxedatoms.set_constraint(constraint)
                    relaxedatoms.calc = GPAW(txt=relaxname + '.txt', **params)
                    opt = BFGS(relaxedatoms,
                               logfile=relaxname + '.log',
                               trajectory=relaxname + '.traj')
                    opt.run(fmax=0.001, smax=0.0002,
                            smask=[0, 0, 0, 0, 0, 0],
                            emin=-np.inf)
                    relaxedatoms = read(relaxname + '.traj')
                    calc = get_wavefunctions(relaxedatoms, relaxnamegpw,
                                             params)
                phase_sc[s] = get_polarization_phase(relaxnamegpw)

            vol = abs(np.linalg.det(oldcell_cv / Bohr))
            dphase_c = phaseclamped_sc[1] - phaseclamped_sc[0]
            # dphase_c -= np.round(dphase_c / (2 * np.pi)) * 2 * np.pi
            dphasedeps_c = dphase_c / (2 * delta)
            eps_v = (-np.dot(dphasedeps_c, oldcell_cv / Bohr) /
                     (2 * np.pi * vol))

            if (~atoms.pbc).any():
                L = np.abs(np.linalg.det(oldcell_cv[~pbc_c][:, ~pbc_c] / Bohr))
                eps_v *= L

            epsclamped_vvv[:, i, j] = eps_v
            epsclamped_vvv[:, j, i] = eps_v
            if world.rank == 0:
                # print(np.round(phaseclamped_sc, 5), 'phase_c')
                print(np.round(dphase_c, 5), 'dphase_c')
                print(np.round(eps_v, 5), 'Clamped eps_v')

            dphase_c = phase_sc[1] - phase_sc[0]
            # dphase_c -= np.round(dphase_c / (2 * np.pi)) * 2 * np.pi
            dphasedeps_c = dphase_c / (2 * delta)
            eps_v = (-np.dot(dphasedeps_c, oldcell_cv / Bohr) /
                     (2 * np.pi * vol))
            if (~atoms.pbc).any():
                eps_v *= L

            eps_vvv[:, i, j] = eps_v
            eps_vvv[:, j, i] = eps_v
            if world.rank == 0:
                print(np.round(dphase_c, 5), 'dphase_c')
                print(np.round(eps_v, 5), 'eps_v')

    if world.rank == 0:
        print('epsclamped_vv')
        print(np.round(epsclamped_vvv[:,
                                      [0, 1, 2, 1, 0, 0],
                                      [0, 1, 2, 2, 2, 1]], 5))
        print('epsclamped_vv nC / m')
        print(np.round(epsclamped_vvv[:,
                                      [0, 1, 2, 1, 0, 0],
                                      [0, 1, 2, 2, 2, 1]] *
                       1.602e-19 / (Bohr * 1e-10) * 1e9, 5))

        print('eps_vv')
        print(np.round(eps_vvv[:,
                               [0, 1, 2, 1, 0, 0],
                               [0, 1, 2, 2, 2, 1]], 5))
        print('eps_vv nC / m')
        print(np.round(eps_vvv[:,
                               [0, 1, 2, 1, 0, 0],
                               [0, 1, 2, 2, 2, 1]] *
                       1.602e-19 / (Bohr * 1e-10) * 1e9, 5))

    data = {'eps_vvv': eps_vvv,
            'epsclamped_vvv': epsclamped_vvv}

    filename = 'piezoelectrictensor.json'

    with paropen(filename, 'w') as fd:
        json.dump(jsonio.encode(data), fd)


if __name__ == '__main__':
    piezoelectrictensor()
