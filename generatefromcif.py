import argparse
from pathlib import Path
import json
from glob import glob

import numpy as np

from ase.io import read, write


def generatefromcif(prototype):
    files = glob('*.cif')
    
    for filepath in files:
        atoms = read(filepath)
        atoms.set_pbc(True)
        atoms.wrap()
        pos_ac = atoms.get_scaled_positions()
        avg_c = np.sum(pos_ac, axis=0) / len(pos_ac)
        pos_ac -= avg_c
        atoms.set_scaled_positions(pos_ac)

        formula = atoms.get_chemical_formula(mode='metal')
        name = '{}-{}'.format(formula, prototype)

        if not Path(name).is_dir():
            Path(name).mkdir()

        starttraj = '{}/start.traj'.format(name)

        if not Path(starttraj).is_file():
            write(starttraj, atoms)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('prototype', help='One of MoS2, CdI2, ...')
    args = parser.parse_args()
    prototype = args.prototype

    if prototype and not Path('info.json').is_file():
        with open('info.json', 'w') as fd:
            fd.write(json.dumps({'prototype': prototype}))

    generatefromcif(prototype=prototype)
