from os.path import exists
from gpaw import GPAW
from gpaw.bztools import find_high_symmetry_monkhorst_pack
from gpaw.response.df import DielectricFunction
from gpaw.mpi import world
import numpy as np


def getdielectricfunction():
    gs = 'gs.gpw'
    density = 20

    if not exists('es.gpw'):
        calc_old = GPAW(gs, txt=None)
        nval = calc_old.wfs.nvalence
        try:
            kpts = find_high_symmetry_monkhorst_pack(gs,
                                                     density=density)
        except RuntimeError:
            kpts = {'density': density, 'gamma': True, 'even': True}

        calc = GPAW(gs, fixdensity=True, kpts=kpts,
                    nbands=4 * nval, convergence={'bands': 5 * nval})
        calc.diagonalize_full_hamiltonian(nbands=4 * nval, expert=True)
        calc.write('es.gpw', mode='all')

    df = DielectricFunction('es.gpw', eta=1e-10, domega0=0.02,
                            integrationmode='tetrahedron integration',
                            name='chi0')

    df1x, df2x = df.get_dielectric_function(direction='x')
    df1y, df2y = df.get_dielectric_function(direction='y')
    df1z, df2z = df.get_dielectric_function(direction='z')

    plasmafreq_vv = df.chi0.plasmafreq_vv

    frequencies = df.get_frequencies()
    data = {'df1x': np.array(df1x),
            'df2x': np.array(df2x),
            'df1y': np.array(df1y),
            'df2y': np.array(df2y),
            'df1z': np.array(df1z),
            'df2z': np.array(df2z),
            'plasmafreq_vv': plasmafreq_vv,
            'frequencies': frequencies}

    filename = 'df.npz'

    if world.rank == 0:
        np.savez_compressed(filename, **data)

if __name__ == '__main__':
    getdielectricfunction()
    
