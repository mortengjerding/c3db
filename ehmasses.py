"""
Note with SOC then symmetry should be off for magnetic materials,
but one can use the full symmetry for non magnetic materials for the
eigenvalues (the spin projection and wave-functions will be wrong though)
"""

import os.path as op
from math import pi

import numpy as np
from ase.dft.bandgap import bandgap
from ase.parallel import paropen
from ase.units import Hartree, Bohr
from ase.io import read
from gpaw import GPAW
from gpaw.kpt_descriptor import to1bz
from c2db.gap import get_kpts_inside_sphere, fit_2d_paraboloid
from c2db.utils import gpw2eigs


def socstr(soc):
    return '_soc' if soc else ''


def runeh(kptdensdens=12.0):
    bg()
    return None  # The stuff below is not working for 3D

    for soc in [True, False]:
        e1e2g, skn1, skn2 = gap(soc=soc, direct=False)
        if e1e2g[2] < 0.01:
            pass
        else:
            nonsc_spheres(soc=soc)

    for soc in [True, False]:
        if op.isfile('sphere_vbm' + socstr(soc) + '.gpw'):
            d, extrema = masstensor(soc)
            eig_vbm, mass_vbm = d['eigenvector_vbm'], d['masstensor_vbm']
            eig_cbm, mass_cbm = d['eigenvector_cbm'], d['masstensor_cbm']
            new_kpts = [adaptive_spheres(eig_vbm, mass_vbm, extrema['vbm']),
                        adaptive_spheres(eig_cbm, mass_cbm, extrema['cbm'])]
            nonsc_spheres(kpts=new_kpts, soc=soc, basename='sphere_adaptive')
            d2, _ = masstensor(soc, basename='sphere_adaptive')
            mass2_vbm = d2['masstensor_vbm']
            mass2_cbm = d2['masstensor_cbm']
            maxdev_vbm = max_deviation(mass_vbm[:2], mass2_vbm[:2])
            maxdev_cbm = max_deviation(mass_cbm[:2], mass2_cbm[:2])
            d2.update(maxdev_vbm=maxdev_vbm, maxdev_cbm=maxdev_cbm)
            with paropen('masstensor' + socstr(soc) + '.npz', 'wb') as f:
                np.savez(f, **d)
            with paropen('masstensor_adapt' + socstr(soc) + '.npz', 'wb') as f:
                    np.savez(f, **d2)


def max_deviation(a, b):
    quotient = np.abs(a / b)
    maxval = max(abs(quotient.max() - 1), abs(quotient.min() - 1))
    return maxval


def bg(gpw='densk.gpw'):
    """find bandgap, vbm, cbm, k_cbm, k_vbm in densk.gpw and save it to
    it to gap.npz and gap_soc.npz
    """
    if not op.isfile(gpw):
        return
    calc = GPAW(gpw, txt=None)
    ibzkpts = calc.get_ibz_k_points()
    for soc in [True, False]:
        e1e2g, skn1, skn2 = gap(soc=soc, direct=False)
        e1e2g_dir, skn1_dir, skn2_dir = gap(soc=soc, direct=True)
        k1, k2 = skn1[1], skn2[1]
        k1_dir, k2_dir = skn1_dir[1], skn2_dir[1]
        k1_c = ibzkpts[k1] if k1 is not None else None
        k2_c = ibzkpts[k2] if k2 is not None else None
        if k1_c is not None:
            k1_c = to1bz(k1_c[None], calc.wfs.gd.cell_cv)[0]
        if k2_c is not None:
            k2_c = to1bz(k2_c[None], calc.wfs.gd.cell_cv)[0]
        k1_dir_c = ibzkpts[k1_dir] if k1_dir is not None else None
        k2_dir_c = ibzkpts[k2_dir] if k2_dir is not None else None
        if k1_dir_c is not None:
            k1_dir_c = to1bz(k1_dir_c[None], calc.wfs.gd.cell_cv)[0]
        if k2_dir_c is not None:
            k2_dir_c = to1bz(k2_dir_c[None], calc.wfs.gd.cell_cv)[0]

        efermi = 333.0
        if soc:
            _, efermi = gpw2eigs(gpw, soc=True)
        else:
            efermi = calc.get_fermi_level()
        data = {'gap': e1e2g[2],
                'vbm': e1e2g[0],
                'cbm': e1e2g[1],
                'gap_dir': e1e2g_dir[2],
                'vbm_dir': e1e2g_dir[0],
                'cbm_dir': e1e2g_dir[1],
                'k1_c': k1_c,
                'k2_c': k2_c,
                'k1_dir_c': k1_dir_c,
                'k2_dir_c': k2_dir_c,
                'skn1': skn1,
                'skn1_dir': skn1_dir,
                'skn2': skn2,
                'skn2_dir': skn2_dir,
                'efermi': efermi}
        with paropen('gap{}.npz'.format(socstr(soc)), 'wb') as f:
            np.savez(f, **data)


def gap(soc, direct=False, gpw='densk.gpw'):
    """Extension of ase.dft.bandgap.bandgap to also work with spin orbit
    coupling and return vbm (e1) and cbm (e2)
    returns (e1, e2, gap), (s1, k1, n1), (s2, k2, n2)
    """
    calc = GPAW(gpw, txt=None)
    if soc:
        e_km, efermi = gpw2eigs(gpw, soc=True)
        gap, km1, km2 = bandgap(eigenvalues=e_km, efermi=efermi, direct=direct,
                                kpts=calc.get_ibz_k_points(), output=None)
        if km1[0] is not None:
            e1 = e_km[km1]
            e2 = e_km[km2]
        else:
            e1, e2 = None, None
        x = (e1, e2, gap), (0,) + tuple(km1), (0,) + tuple(km2)
    else:
        g, skn1, skn2 = bandgap(calc, direct=direct, output=None)
        if skn1[1] is not None:
            e1 = calc.get_eigenvalues(spin=skn1[0], kpt=skn1[1])[skn1[2]]
            e2 = calc.get_eigenvalues(spin=skn2[0], kpt=skn2[1])[skn2[2]]
        else:
            e1, e2 = None, None
        x = ((e1, e2, g), skn1, skn2)
    return x


def nonsc_spheres(kpts=(100, 100, 1), soc=False, emptybands=20,
                  basename='sphere'):
    """sample around vbm and cbm in k-space
    """
    convbands = int(emptybands / 2)
    e1e2g, skn1, skn2 = gap(soc)
    e1, e2, g = e1e2g
    if len(np.array(kpts).shape) == 1:
        calc = GPAW('densk.gpw', txt=None)

        def get_kpts_xk(calc, kpts, kr=0.015):
            ibzkpts = calc.get_ibz_k_points()
            rcell_cv = calc.atoms.get_reciprocal_cell() * 2 * pi
            krad = kr * np.linalg.norm(rcell_cv[0])  # XX consider rcell_cv[1]?
            kgrid = np.asarray(kpts, int)
            k2add_kc = get_kpts_inside_sphere(rcell_cv, kgrid, krad)
            k1_c, k2_c = ibzkpts[[skn1[1], skn2[1]]]
            k1_kc = k1_c + k2add_kc
            k2_kc = k2_c + k2add_kc
            return k1_kc, k2_kc

        k1_kc, k2_kc = get_kpts_xk(calc, kpts)

        if abs(k1_kc).max() > 1 or abs(k2_kc).max() > 1:
            raise ValueError('Very strange k-points: {}, {}'
                             .format(k1_kc, k2_kc))
    else:
        k1_kc, k2_kc = kpts

    name = basename + '_cbm' + socstr(soc)
    if not op.isfile(name + '.gpw'):  # XXX for testing
        calc = GPAW('gs.gpw',
                    nbands=-emptybands,
                    fixdensity=True,
                    kpts=k2_kc,
                    symmetry='off',
                    txt=name + '.txt',
                    convergence={'bands': -convbands})
        calc.get_atoms().get_potential_energy()
        calc.write(name + '.gpw')

    name = basename + '_vbm' + socstr(soc)
    if not op.isfile(name + '.gpw'):  # XXX for testing
        calc = GPAW('gs.gpw',
                    nbands=-emptybands,
                    fixdensity=True,
                    kpts=k1_kc,
                    symmetry='off',
                    txt=name + '.txt',
                    convergence={'bands': -convbands})
        calc.get_potential_energy()
        calc.write(name + '.gpw')


def adaptive_spheres(eigenvectors, masses, extremum=np.array((0, 0)),
                     npoints=5, soc=False, cell=None):

    if cell is None:
        cell = read('densk.gpw').cell

    def ellipse_from_circle(circle, energy_range=5e-4):
        r2 = 2 * energy_range / Hartree
        circle[:, 0] *= np.sqrt(np.abs(masses[0]) * r2)
        circle[:, 1] *= np.sqrt(np.abs(masses[1]) * r2)
        return circle

    def rotate_ellipse(ellipse):
        return np.dot(eigenvectors[:2, :2], ellipse.T).T

    axis = np.linspace(-1, 1, npoints)
    X, Y = np.meshgrid(axis, axis)
    indices = X**2 + Y**2 <= 1
    circle = np.array([X[indices], Y[indices]]).T
    ellipse_kv = rotate_ellipse(ellipse_from_circle(circle))
    ellipse_kv += extremum
    ellipse_kv = np.append(ellipse_kv, np.zeros((len(ellipse_kv), 1)), axis=1)
    ellipse_kc = np.dot(ellipse_kv, cell.T) / (2 * pi) / Bohr
    return ellipse_kc


def find_minimum(coefficients):
    A = coefficients
    k_vy = ((A[2] * A[3] - 2 * A[0] * A[4]) /
            (4 * A[0] * A[1] - A[2] ** 2))
    k_vx = - A[3] / (2 * A[0]) - A[2] * k_vy / (2 * A[0])
    return np.array([k_vx, k_vy])


def masstensor(soc, basename='sphere'):
    """
    calculate masstensor from calculations with sampling around
    band max/min k kpts
    """
    e1e2g, skn1, skn2 = gap(soc)
    d = {}
    extrema = {}

    for (s, k, n), x in zip((skn1, skn2), ('vbm', 'cbm')):
        gpw = basename + '_' + x + socstr(soc) + '.gpw'
        calc = GPAW(gpw, txt=None)
        e_skm, _ = gpw2eigs(gpw, soc=soc)
        if e_skm.ndim != 3:
            e_skm = e_skm[np.newaxis]
        b_k = e_skm[s, :, n] / Hartree
        kpts_kc = calc.get_ibz_k_points()
        rcell_cv = calc.atoms.get_reciprocal_cell() * 2 * pi
        kpts_kv = np.dot(kpts_kc, rcell_cv)
        w, V, A = fit_2d_paraboloid(kpts_kv * Bohr, b_k)
        extrema[x] = find_minimum(A)

        # w: slopes along x,y,z
        d['masstensor_' + x] = 0.5 / w
        d['eigenvector_' + x] = V
    return d, extrema


if __name__ == '__main__':
    runeh()
